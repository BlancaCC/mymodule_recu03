class mymodule::apache_config{

class { 'apache': } 
### només un dels 2, o el de dalt o el de baix

#package {'apache':
# name => $apache,
# ensure => latest,
#}

#VH
apache::vhost{'centos.dev':
  port => '80',
  docroot => '/var/www/',
}

apache::vhost{'project.dev':
  port => '80',
  docroot => '/var/www/project',
}

# Ip Tables.
# firewalld - Centos 7
firewalld_rich_rule { 'Accept HTTP':
 ensure  => present,
 zone    => 'public',
 service => 'http',
 action  => 'accept',
}

file { '/var/www/index.html':
ensure => 'present',
  mode => '0644',
  source => "/vagrant/files/htmls/my_index.html",
}
file { '/var/www/project/index.html':
  ensure => file,
# owner => "root",
# group => "root",
  replace => true,
  source => "/vagrant/files/htmls/my_index_project.html",
}

###PHP 5.6
include ::yum::repo::remi
package { 'libzip-last':
  require => Yumrepo['remi']
}

$php_version = '56'

  #remi_php55 requires the remi repo as well
  if $php_version == '55' {
    $yum_repo = 'remi-php55'
    include ::yum::repo::remi_php55
  }
  # remi_php56 requires the remi repo as well
elsif $php_version == '56' {
  $yum_repo = 'remi-php56'
  class{'::yum::repo::remi_php56':
    require => Package['libzip-last']
  }
}
  # version 5.4
elsif $php_version == '54' {
  $yum_repo = 'remi'
  include ::yum::repo::remi
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo],
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'pecl-memcache', 'soap' ]: }

}