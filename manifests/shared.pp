class mymodule::shared{

host { 'localhost':
	ensure       => 'present',
	target       => '/etc/hosts',
	ip           => '127.0.0.1',
	host_aliases => ['mysql1', 'memcached1']
}

# Ensure Time Zone and Region.
class { 'timezone':
  timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
  server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
}

# Miscellaneous packages.
$misc_packages = [
  'sendmail','vim-enhanced','telnet','zip','unzip','screen',
  'libssh2','libssh2-devel','gcc','gcc-c++','autoconf','automake','postgresql-libs']

 package { $misc_packages: ensure => latest }

# EPEL repository
yumrepo {'EPEL':
	baseurl => 'http://dl.fedoraproject.org/pub/epel/7/x86_64',
	enabled => 1,
	gpgcheck => 0
}
 
}